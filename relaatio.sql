drop database if exists todo;
create database
if not EXISTS todo;
use todo;


create table task
(
    id int
    AUTO_INCREMENT primary key,
    description VARCHAR
    (255) not null,
    done boolean default false,
    added TIMESTAMP default CURRENT_TIMESTAMP
)