<?php
$db = null;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tehtävälista</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body>
    <div class="content">
        <?php
        try {
            $db = new PDO('mysql:host=localhost;dbname=todo;charset=utf8', 'root', '');
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $pdex) {
            print "<p>Tietokannan avaus epäonnistui. " . $pdex->getMessage() . "</p>";
        }
        ?>